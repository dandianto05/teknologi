
--query 1
SELECT batch258.employees.first_name as SALES, COUNT(batch258.orders.status) as JUMLAH_PENJUALAN
    FROM batch258.employees
    JOIN batch258.orders ON batch258.employees.employee_id = batch258.orders.salesman_id
    WHERE status = 'Shipped'
    GROUP BY batch258.employees.first_name
    ORDER BY COUNT(batch258.orders.status) desc;
    

        
--query 2
SELECT e2.first_name AS MANEGER, e1.first_name AS SALES, COUNT(batch258.orders.status) as JUMLAH_PENJUALAN_SALES
        FROM batch258.employees e1
        LEFT JOIN batch258.employees e2 ON e1.manager_id = e2.employee_id
        JOIN batch258.orders ON e1.employee_id = batch258.orders.salesman_id
        WHERE status = 'Shipped'
        GROUP BY e1.first_name, e2.first_name
        ORDER BY COUNT(batch258.orders.status) desc;
        

    

--create packages function
CREATE OR REPLACE PACKAGE batch258.terbaik AS
    FUNCTION sales_rangking 
    RETURN SYS_REFCURSOR;
    
    FUNCTION manager_rangking 
    RETURN SYS_REFCURSOR;
    
END terbaik;

--create package body
CREATE OR REPLACE PACKAGE BODY batch258.terbaik AS
    --function sales
    FUNCTION sales_rangking 
        RETURN SYS_REFCURSOR
    IS
        c_sales SYS_REFCURSOR;
    BEGIN
        OPEN c_sales FOR 
        SELECT batch258.employees.first_name as SALES
        FROM batch258.employees
        JOIN batch258.orders ON batch258.employees.employee_id = batch258.orders.salesman_id
        WHERE status = 'Shipped'
        GROUP BY batch258.employees.first_name
        ORDER BY COUNT(batch258.orders.status) desc;
        RETURN c_sales;
    END sales_rangking;   
    
    --function manager
    FUNCTION manager_rangking 
        RETURN SYS_REFCURSOR
    IS
        c_manager SYS_REFCURSOR;
    BEGIN
        OPEN c_manager FOR 
        
        SELECT e2.first_name AS MANEGER
        FROM batch258.employees e1
        LEFT JOIN batch258.employees e2 ON e1.manager_id = e2.employee_id
        JOIN batch258.orders ON e1.employee_id = batch258.orders.salesman_id
        WHERE status = 'Shipped'
        GROUP BY e2.first_name
        ORDER BY COUNT(batch258.orders.status) desc;
        
        RETURN c_manager;
    END manager_rangking; 
    
END terbaik;

--call packages
SELECT
    batch258.terbaik.sales_rangking AS SALES_RANGKING,
    batch258.terbaik.manager_rangking AS MANAGER_RANGKING
FROM dual;