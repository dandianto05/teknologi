--TUGAS 1
CREATE OR REPLACE PROCEDURE batch258.getcustomers_by_region
AS
     c_customers SYS_REFCURSOR;
BEGIN
    OPEN c_customers FOR
        SELECT customer_id, name
         FROM batch258.customers

         JOIN batch258.orders USING (customer_id)
         JOIN batch258.order_items USING (order_id)
         JOIN batch258.products USING (product_id)
         JOIN batch258.inventories USING (product_id)
         JOIN batch258.warehouses USING (warehouse_id)
         JOIN batch258.locations USING (location_id)
         JOIN batch258.countries USING (country_id)
         JOIN batch258.regions USING (region_id)
    
         WHERE region_name = 'Asia';
          
    DBMS_SQL.return_result (c_customers);
EXCEPTION
    WHEN OTHERS THEN
    dbms_output.put_line( SQLERRM );
END;

--call procedure
BEGIN
   batch258.getcustomers_by_region;
END;







--TUGAS 2
CREATE OR REPLACE FUNCTION batch258.total_belanja(
    id_cus PLS_INTEGER)
RETURN NUMBER
IS
    l_total_belanja  NUMBER :=0;
BEGIN
    --get total belanja
    SELECT SUM ( unit_price * quantity)
    INTO l_total_belanja
    FROM batch258.order_items
    JOIN batch258.orders Using (order_id)
    JOIN batch258.customers Using (customer_id)
    WHERE batch258.orders.status = 'Shipped'
    GROUP BY (customer_id)
    HAVING(customer_id) = id_cus;
    
    RETURN l_total_belanja;
END;


BEGIN
    DBMS_OUTPUT.put_line(batch258.total_belanja(23));
END;






--TUGAS 3
CREATE OR REPLACE FUNCTION batch258.product_laku(
    name_region VARCHAR2)
RETURN SYS_REFCURSOR
AS
    l_product SYS_REFCURSOR;
BEGIN
    --get max product quantity 
    OPEN l_product FOR 
        SELECT product_name FROM (
        SELECT * FROM batch258.products
        JOIN batch258.order_items USING (product_id) 
        WHERE batch258.order_items.quantity = (SELECT MAX( batch258.order_items.quantity) FROM batch258.order_items)
        )   JOIN batch258.order_items USING (product_id)
            JOIN batch258.inventories USING (product_id)
            JOIN batch258.warehouses USING (warehouse_id)
            JOIN batch258.locations USING (location_id)
            JOIN batch258.countries USING (country_id)
            JOIN batch258.regions USING (region_id)
            WHERE region_name = name_region
            GROUP BY (product_name);
         
        RETURN l_product;
END;

--call function
DECLARE
   l_product SYS_REFCURSOR;
   l_product_name batch258.products.product_name%TYPE;
BEGIN
   -- get the ref cursor from function product laku
   l_product := batch258.product_laku('Asia'); 
   
   -- process each employee
   LOOP
      FETCH
         l_product
      INTO
         l_product_name;
      EXIT
   WHEN l_product%notfound;
      dbms_output.put_line(l_product_name);
   END LOOP;
   -- close the cursor
   CLOSE l_product;
END;
